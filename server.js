// ##########################
// Dependancies
// ##########################
var express = require('express');
var eloRank = require('elo-rank');
var fs = require('fs');

// ##########################
// Constants and Variables
// ##########################
// configuration constants
var dataFolder = __dirname + '/data/';

// app variables
var players = {};
var recentGames = [];
var tableStatus = 'FREE';

// ##########################
// Ranking Setup
// ##########################
var elo = new eloRank();

// ##########################
// Server Setup
// ##########################
var app = express();
var serverPort = 3000;
app.use(express.static(__dirname+'/webpage'));

// ##########################
// Initial Data Load
// ##########################
players = loadDataFile(dataFolder + 'players.txt');

// ##########################
// Express Endpoints
// ##########################
// creates a new player object
app.get('/addPlayer', function (req, res) {
    var response = '';
    if (req.query.name) {
        var newName = req.query.name;
        if (players[newName]) {
            response = 'Player already exists';
        }
        else {
            players[newName] = {
                name : newName,
                score : 1000,
                wins : 0,
                losses : 0,
                games : []
            };
            updatePlayers(players);
            response = JSON.stringify(players);
        }
    }
    else {
        response = 'Invalid parameters.  Parameter "name" is required';
    }
    res.end(response);
});

// returns all players
app.get('/getPlayers', function(req, res) {
    var response = JSON.stringify(players);
    res.end(response);
});

app.get('/postGame', function(req, res) {
    var winner = req.query.winner;
    var loser = req.query.loser;
    if (!winner || !loser) {
        res.end('Invalid parameters.  Parameters "winner" and "loser" are required');
        return;
    }
    // check if this game has been logged too recently
    if (checkArray(recentGames, winner+loser)) {
        res.end(JSON.stringify({error:'Game recently posted'}));
        return;
    }
    var winnerScore = players[winner].score;
    var loserScore = players[loser].score;
    var expectedWinnerScore = elo.getExpected(winnerScore, loserScore);
    var expectedLoserScore = elo.getExpected(loserScore, winnerScore);
    players[winner].score = elo.updateRating(expectedWinnerScore, 1, winnerScore);
    players[winner].wins++;
    players[loser].score = elo.updateRating(expectedLoserScore, 0, loserScore);
    players[loser].losses++;
    
    var gamePointChange = players[winner].score - winnerScore;
    players[winner].games.unshift(
        {
            date : new Date().today(),
            opponent : loser,
            pointChange : gamePointChange
        }
    );
    players[loser].games.unshift(
        {
            date : new Date().today(),
            opponent : winner,
            pointChange : -gamePointChange
        }
    );
    
    res.end(JSON.stringify(players));
    updatePlayers(players);
    updateLog(winner, loser);
    
    // update recent games tracker and clear it after 5 minutes
    recentGames.push(winner+loser);
    setTimeout(function() {
        removeFromArray(recentGames, winner+loser);
    }, 300000);
});

app.get('/getOdds', function(req, res) {
    var player1 = req.query.player1;
    var player2 = req.query.player2;
    if (!player1 || !player2) {
        res.end('Invalid parameters.  Parameters "winner" and "loser" are required');
        return;
    }
    var player1Score = players[player1].score;
    var player2Score = players[player2].score;
    var player1ExpectedScore = elo.getExpected(player1Score, player2Score);
    var player2ExpectedScore = elo.getExpected(player2Score, player1Score);
    var response = {};
    response[player1] = player1ExpectedScore;
    response[player2] = player2ExpectedScore;
    res.end(JSON.stringify(response));
})

app.get('/getLog', function(req, res) {
    var date = req.query.date;
    if (date) {
        var response =  loadDataFile(dataFolder + 'logs/' + date + '.txt');
        res.end(JSON.stringify(response));
    }
    else {
        var response = loadDataFile(dataFolder + 'logs/' + new Date().today() + '.txt');
        res.end(JSON.stringify(response));
    }
});

app.get('/getTableStatus', function(req, res) {
    res.end(tableStatus);
});

// returns webpage frontend
app.get('/', function(req, res){
    res.sendFile(__dirname+'/index.html');
});

// Receive data from table sensor
app.get('/sensorData', function(req, res) {
    console.log(req.query);
    if (req.query.occupied == 'true') {
        tableStatus = 'IN USE';
    }
    else if (req.query.occupied == 'false') {
        tableStatus = 'FREE';
    }
    res.end('OK');
});


// start server
app.listen(serverPort, function () {
    console.log('App listening on port '+serverPort);
});

// ##########################
// Helper Functions
// ##########################
// creates a new entry for new data and writes it to file
function updateLog(winner, loser) {
    var currentFile = dataFolder + '/logs/' + new Date().today() + '.txt';
    var dailyLog = loadDataFile(currentFile);
    var dataToPush = {
        timeStamp : new Date().today() + " @ " + new Date().timeNow(),
        winner : winner,
        loser : loser
    };
    dailyLog.push(dataToPush);
    writeDataFile(currentFile, dailyLog);
}

// update players file
function updatePlayers (data) {
    var currentFile = dataFolder + 'players.txt';
    writeDataFile(currentFile, data);
}

// write data to given file
// if file doesn't exist, create it
function writeDataFile(filePath, data) {
    fs.writeFile(filePath, JSON.stringify(data), function(err) {
        if (err) {
            console.log('write error: ' +err.message);
        }
    });
}

// load data from file if file exists
function loadDataFile(filePath) {
    var readData = [];
    if (fs.existsSync(filePath)) {
        readData = JSON.parse(fs.readFileSync(filePath).toString());
    }
    return readData;
}

// checks if object is in array, returns true or false
function checkArray(array, entry) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == entry) {
            return true;
        }
    }
    return false;
}

// removes an object from an array, returns true if object was removed or false
function removeFromArray(array, entry) {
    for (var i=0; i < array.length; i++) {
        if (array[i] == entry) {
            array.splice(i,1);
            return true;
        }
    }
    return false;
}

// For todays date;
Date.prototype.today = function () {
    return (((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"+ ((this.getDate() < 10)?"0":"") + this.getDate() +"-"+ this.getFullYear();
    //return ((this.getDate() < 10)?"0":"") + this.getDate() +"-"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"-"+ this.getFullYear();
}

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}