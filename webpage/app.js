// Custom Filters
Vue.filter('roundDecimal', function (value, input) {
    return Math.round(value * (10*input)) / (10*input);
});

// Main Vue Instance
new Vue({
    el:'#app',
    data: {
        players: [],
        detailMode: 'Stats',
        selectedPlayer1 : {},
        selectedPlayer2: {},
        winner: '',
        loser: '',
        postingError: '',
        tableStatus: 'FREE'
    },
    computed: {
        rankedPlayers: function () {
            var sortedArray = [];
            var hasPlayed = [];
            var hasNotPlayed = [];
            this.players.forEach(function (player) {
                if (player.wins != 0 || player.losses != 0) {
                    hasPlayed.push(player);
                } else {
                    hasNotPlayed.push(player);
                }
            });
            hasPlayed.sort(function (a, b) {
                return b.score - a.score;
            });
            sortedArray = sortedArray.concat(hasPlayed);
            sortedArray = sortedArray.concat(hasNotPlayed);
            return sortedArray;
        },
        displayedGames: function () {
            var self = this;
            if (this.selectedPlayer2.name == undefined) {
                return this.selectedPlayer1.games;
            } 
            return this.selectedPlayer1.games.filter(function (game) {
                return game.opponent == self.selectedPlayer2.name;
            });
        },
        playerStats: function () {
            var self = this;
            var stats = {
                totalChange: 0,
                wins: 0,
                losses: 0,
                totalGames: 0,
                winRate: 0
            };
            if (self.selectedPlayer1.name != undefined) {
                if (self.selectedPlayer2.name != undefined) {
                    self.selectedPlayer1.games.forEach(function (game) {
                        if (game.opponent == self.selectedPlayer2.name) {
                            stats.totalChange += game.pointChange;
                            stats.wins += game.pointChange > 0 ? 1 : 0;
                            stats.losses += game.pointChange < 0 ? 1 :0;
                            stats.totalGames++;
                        }
                    });
                    stats.winRate = stats.wins != 0 ? (stats.wins / (stats.wins + stats.losses)) * 100 : 0;
                } else {
                    self.selectedPlayer1.games.forEach(function (game) {
                        stats.totalChange += game.pointChange;
                        stats.wins += game.pointChange > 0 ? 1 : 0;
                        stats.losses += game.pointChange < 0 ? 1 :0;
                        stats.totalGames++;
                    });
                    stats.winRate = stats.wins != 0 ? (stats.wins / (stats.wins + stats.losses)) * 100 : 0;
                }
            }
            return stats;
        },
        allPlayerStats: function () {
            var self = this;
            var players = {};
            if (self.selectedPlayer1.name != undefined) {
                self.selectedPlayer1.games.forEach(function (game) {
                    if (players[game.opponent] == undefined) {
                        players[game.opponent] = {
                            name: game.opponent,
                            wins: game.pointChange > 0 ? 1 : 0,
                            losses: game.pointChange < 0 ? 1 :0
                        }                    
                    } else {
                        players[game.opponent].wins += game.pointChange > 0 ? 1 : 0;
                        players[game.opponent].losses += game.pointChange < 0 ? 1 : 0;
                    }
                });
                var playersArray = Object.values(players);
                playersArray.sort(function (a, b) {
                    return (b.wins + b.losses) - (a.wins + a.losses);
                });
                return playersArray;
            } else {
                return [];
            }
        }
    },
    mounted () {
        this.getPlayers();
    },
    methods: {
        getPlayers () {
            var self = this;
            $.get('/getPlayers', function (data) {
                self.players = generatePlayerArray(JSON.parse(data));
            }); 
        },
        playerClickFunction (name) {
            if (this.detailMode == 'Stats' || 'Games') {
                if (name != this.selectedPlayer1.name) {
                    this.selectedPlayer1 = getPlayerFromArray(name, this.players);
                }
                else {
                    this.selectedPlayer1 = {};
                }
                this.selectedPlayer2 = {};
            }
        },
        changeDetailMode (newMode) {
            if (newMode != this.detailMode) {
                this.detailMode = newMode;
                this.selectedPlayer2 = {};
            }
        },
        showArrow (change) {
            if (change > 0) {
                return 'green_arrow.png';
            }
            else {
                return 'red_arrow.png';
            }
        },
        chooseSelectedPlayer2 (newName) {
            if (this.selectedPlayer2.name != newName) {
                this.selectedPlayer2 = {name: newName};
            } else {
                this.selectedPlayer2 = {};
            }
        },
        clearSelectedPlayer2 () {
            this.selectedPlayer2 = {};
        },
        postGame () {
            var self = this;
            var params = '?winner=' + self.winner + '&loser=' + self.loser;
            $.get('/postGame' + params, function (data) {
                data = JSON.parse(data);
                if (data.error) {
                    self.postingError = data.error;
                } else {
                    self.players = generatePlayerArray(data);
                    if (self.selectedPlayer1.name != undefined) {
                        self.selectedPlayer1 = getPlayerFromArray(self.selectedPlayer1.name, self.players);
                    }
                    self.winner = null;
                    self.loser = null;
                    self.postingError = '';
                }
            });
        }
    }
});

// Helper Methods
function getPlayerFromArray (name, playersArray) {
    for (var i = 0; i < playersArray.length; i++) {
        if (name == playersArray[i].name) {
            return playersArray[i]
        }
    }
    return null;
}

function generatePlayerArray (data) {
    var array = [];
    for (key in data) {
        if (data.hasOwnProperty(key)) {
            array.push(data[key]);
        }
    }
    return array;
}