Vue.component('compare-stat', {
    props: ['name', 'wins', 'losses'],
    computed: {
        winRate : function () {
            return this.wins != 0 ? (this.wins / (this.wins + this.losses)) * 100 : 0;
        }
    },
    template: `
        <div class="row compStat-player">
            <div class="col-md-3">
                <h4>{{name}}</h4>
            </div>
            <div class="col-md-7 row">
                <div class="row compStat-gameRow">
                    <div class="compStat-gameSubRow">
                        <div class="compStat-game compStat-win" v-for="n in wins"></div>
                    </div>
                    <div class="compStat-gameSubRow">
                        <div class="compStat-game compStat-loss" v-for="n in losses"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <h4>{{winRate | roundDecimal(1)}}%</h4>
            </div>
        </div>
    `
});